// requires
const Discord = require('discord.js');
const GoogleTTS = require('@google-cloud/text-to-speech');
const Readable = require('stream').Readable;
//const Ytdl = require('ytdl-core');
//const YtdlDiscord = require('ytdl-core-discord');
require('ffmpeg');

const fs = require('fs');
const util = require('util');

// Bot
const Bot = new Discord.Client();

// StreamDispatcher
var Dispatcher = null;

var DictJSON = null;

// Environment File
const DotEnv = require('dotenv');
DotEnv.config();

// TextToSpeech Client
const tts = new GoogleTTS.TextToSpeechClient({
  credentials: {
    client_email: process.env.GOOGLE_EMAIL,
    private_key: process.env.GOOGLE_KEY.replace(/\\n/g, '\n')
  }
});

function NameReplace(user)
{
	let nameReplace = user.username;
	if(DictJSON[user.id])
	{
		nameReplace = DictJSON[user.id];
		console.log("found name replacement");
	}
	return nameReplace;
}

function GetTimeStamp()
{
	let time = new Date();
	
	let year = time.getFullYear();
	let month = ("0" + (time.getMonth() + 1)).slice(-2);
	let date = ("0" + time.getDate()).slice(-2);
	
	let hours = time.getHours();
	let minutes = time.getMinutes();
	let seconds = time.getSeconds();
	
	return (`[${year}-${month}-${date} ${hours}:${minutes}:${seconds}]`);
}

// テキスト → base64(mp3)
async function TextToSpeechBase64(text, voiceType, voicePitch, voiceSpeed) {
	let isEnglish = true;
	var english = /^[A-Za-z0-9]*$/;
	for(var i = 0; i < text.length; i++)
	{
        if(!english.test(text[i]))
		{
			isEnglish = false;
			break;
		}
    }

	let langCode = 'ja-JP';
	let voiceName = '';
	switch(voiceType){
		case 1:
			voiceName = 'ja-JP-Wavenet-A';
		break;
		
		case 2:
			voiceName = 'ja-JP-Wavenet-B';
		break;
		
		case 3:
			voiceName = 'ja-JP-Wavenet-C';
		break;
		
		case 4:
			voiceName = 'ja-JP-Standard-A';
		break;
		
		case 5:
			voiceName = 'ja-JP-Standard-B';
		break;
		
		case 6:
			voiceName = 'ja-JP-Standard-C';
		break;
		
		case 7:
			voiceName = 'ja-JP-Standard-D';
		break;
		
		default:
			voiceName = 'ja-JP-Wavenet-D';
	}
	
	if(isEnglish)
	{
		langCode = 'en-US';
		voiceName = 'en-US-Wavenet-D';
	}
	
	console.log(GetTimeStamp() + "ACTION: TTS Start");
	let request = {
		input: {text},
		voice: {
			languageCode: langCode,
			name: voiceName
		},
		audioConfig: {
			audioEncoding: 'MP3',
			speakingRate: voiceSpeed,
			pitch: voicePitch
		}
	};
	console.log(GetTimeStamp() + "ACTION: Google request message created");
	
	let [response] = await tts.synthesizeSpeech(request);
	console.log(GetTimeStamp() + "ACTION: received response");
	let buffer = Buffer.from(response.audioContent.buffer).toString('base64');
	let audio = `data:‎audio/mpeg;base64,${buffer}`;
	
	let stream = new Readable();
	stream._read = () => {};
	stream.push(response.audioContent);
	stream.push(null);
	console.log(GetTimeStamp() + "ACTION: created readable stream");
	
	return stream;
}

function ConnectToVC(client) {
	let channel = client.channels.fetch(process.env.DISCORD_VOICE_CH_ID)
	.then(channel => {
		console.log(GetTimeStamp() + "VC: found channel: " + channel.name);
		
		// If channel is joinable
		if(channel.joinable)
		{
			channel.join()
				.then(connection => 
				{
					console.log(GetTimeStamp() + "VC: connected to vc!");
				})
				.catch(DumpError)
		}
	})
	.catch(DumpError);
}

function DisconnectFromVC(client, connection) {
	let channel = client.channels.fetch(process.env.DISCORD_VOICE_CH_ID)
	.then(channel => {
		console.log(GetTimeStamp() + "VC: found channel: " + channel.name);
		if(!connection){ console.log(GetTimeStamp() + "WARN: no connection found"); return; }
		connection.disconnect();
	})
	.catch(DumpError);
}

function DictionaryJSON() {
	fs.readFile('Dictionary.json', (err, data) => {
		if (err) DumpError(err)
		DictJSON = JSON.parse(data);
		console.log("read json file");
	});
}

// Start Log	
Bot.on('ready', () => {
	DictionaryJSON();
	
	console.log(GetTimeStamp() + "LOG: Bot is online");
	Bot.user.setPresence({
		activity: {
			name: 'Bot author @BDtetra'
		},
		status: 'online',
	});
	ConnectToVC(Bot);
});

// On Voice State Update
Bot.on('voiceStateUpdate', async (oldState, newState) =>
{
	console.log("");
	console.log("----VOICE START----");
	// Find the "Log" Channel
	let logChannel = newState.guild.channels.cache.find(ch => ch.name === 'log');

	if(!logChannel) { console.log(GetTimeStamp() + "WARN: Log channel not found"); return; }
	
	
	console.log(GetTimeStamp() + "LOG: " + logChannel.name + " channel found");
	
	// User Joins a voice channel 
	if(oldState.channel === null && newState.channel !== null)
	{
		let user = newState.member.user;
		
		let embed = {
			"author": {
			  "name": user.username,
			  "icon_url": user.displayAvatarURL('png'),
			},
			"color": 8409131,
			"description": "@" + user.username + " joined Channel",
		};
		
		logChannel.send({ embed }).catch(DumpError);
		console.log(GetTimeStamp() + "ACTION: sent embed message");
		
		let connection = newState.guild.voice.connection;
		console.log(GetTimeStamp() + "LOG: " + user.username + " entered channel");
		
		// anita専用トラップ
		if(user.id == 105674789196206080)
		{
			if(!connection){ console.log(GetTimeStamp() + "WARN: no connection found"); return; }
			Dispatcher = connection.play('famima.wav', { volume: 0.5 });
			console.log(GetTimeStamp() + "ACTION: TTS done");
		}
		// その他
		else
		{
			let text = NameReplace(user) + "がチャンネルに入りました。";
			
			await ReadTTS(Bot, connection, text);
		}
	}
	
	// User leaves a voice channel
	else if(oldState.channel !== null && newState.channel === null)
	{
		let user = oldState.member.user;
		
		let embed = {
			"author": {
			  "name": user.username,
			  "icon_url": user.displayAvatarURL('png'),
			},
			"color": 8409131,
			"description": "@" + user.username + " left Channel",
		};
		
		logChannel.send({ embed }).catch(DumpError);
		console.log(GetTimeStamp() + "ACTION: sent embed message");
		
		let connection = newState.guild.voice.connection;
		let text = NameReplace(user) + "がチャンネルを退出しました。";
		console.log(GetTimeStamp() + "LOG: " + user.username + " left channel");
		
		await ReadTTS(Bot, connection, text);
	}
	
	// User moves from a voice channel
	else if(oldState.channel.name !== newState.channel.name)
	{
		let user = newState.member.user;
		
		let embed = {
			"author": {
			  "name": user.username,
			  "icon_url": user.displayAvatarURL('png'),
			},
			"color": 8409131,
			"description": "@" + user.username + " moved Channel " + oldState.channel.name + " -> " + newState.channel.name,
		};
		
		logChannel.send({ embed }).catch(DumpError);
		console.log(GetTimeStamp() + "ACTION: sent embed message");
		
		let connection = newState.guild.voice.connection;
		let text = NameReplace(user) + "がチャンネルを移動しました。";
		console.log(GetTimeStamp() + "LOG: " + user.username + " moved channel");
		
		await ReadTTS(Bot, connection, text);
	}
});


Bot.on('message', async (message) => 
{
	console.log("");
	console.log("---MESSAGE START---");
	
	if(message.author.bot) { console.log(GetTimeStamp() + "WARN: message author is bot"); return; }
	
	let guild = message.guild;
	let channel = message.member.voiceChannel;
	
	console.log(GetTimeStamp() + "LOG: message.content");
	
	// if auto-emote reaction channel
	if(message.channel.id === process.env.DISCORD_EMOTE_CH_ID)
	{
		message.react('706010772136001566')
			.then(() => message.react('706010813449895976'));
		return;
	}

	if (message.content.includes('2chan.net'))
	{
		let regex = /^(?:(?:https?\:)?\/\/)?((?:www|m)\.)?.*src\/(?<name>\d+)\.(?<ext>jpg|png|bmp|gif|jpeg|webm|webp|mp3|mp4|swf|mpeg|mpg|avi)/;
		let match = message.content.match(regex);

		if (match == null || match[0] == null) {
			console.log("[REGEX]: no match");
		}
		else {
			console.log("[REGEX]:" + match);
			console.log("[REGEX]:<url>" + match[0]);
			console.log("[REGEX];<name>" + match.groups["name"]);
			console.log("[REGEX]:<ext>" + match.groups["ext"]);

			let name = match.groups["name"].toString();
			let ext = match.groups["ext"].toString();
			let author = message.author;

			switch (ext) {
				case "jpg":
				case "jpeg":
				case "png":
				case "bmp":
				case "gif":
				case "webp":
					console.log("[REGEX]:detected image format");
					message.channel.send({
						embed: {
							author: {
								name: author.username,
								icon_url: author.displayAvatarURL('png'),
							},
							description: match[0],
							//image: {
							//	url: "attachment://name." + ext
							//}
						},
						files: [{
							attachment: match[0].toString(),
							name: name + "." + ext
						}]
					}).catch(DumpError);
					message.delete().catch(DumpError);

					break;

				case "webm":
				case "mp4":
				case "mpeg":
				case "mpg":
				case "avi":
					console.log("[REGEX]:detected video format");
					message.channel.send({
						embed: {
							author: {
								name: author.username,
								icon_url: author.displayAvatarURL('png'),
							},
							description: match[0],
							//video: {
							//	url: "attachment://name." + ext
							//}
						},
						files: [{
							attachment: match[0].toString(),
							name: name + "." + ext
						}]
					}).catch(DumpError);
					message.delete().catch(DumpError);

					break;

				case "mp3":
					break;
			}
        }
    }
	
	if(!guild.voice) { console.log(GetTimeStamp() + "WARN: no guild.voice found"); return; }
	
	// VoiceConnection
	let connection = guild.voice.connection;
	
	// parse the text with a space
	let parsed = message.content.split(' ');
		
	console.log(GetTimeStamp() + "MESG:" + message.content);
	
	if(!parsed) { console.log(GetTimeStamp() + "WARN: no message found"); return; }
	if (!parsed[0]) { console.log(GetTimeStamp() + "WARN: failed to parse"); return; }
	
	switch(parsed[0])
	{
		case "!help":
			console.log(GetTimeStamp() + "CMD: help");
			let embed = new Discord.MessageEmbed()
				.setColor('#0099ff')
				.setAuthor(Bot.user.username, Bot.user.displayAvatarURL('png'))
				.setTitle('All Command List')
				.addFields(
					{ name: '!connect', value: 'BOTの接続'},
					{ name: '!disconnect', value: 'BOTの切断'},
					{ name: '!re', value: 'BOT再接続' },
					{ name: '!test', value: 'ボイステスト' },
					{ name: '!sc {value}', value: 'サウンドクリップ(!scで詳細)' },
					{ name: '!{X}d{Y}', value: 'ダイス' },
					{ name: '[{X}, {Y.y}, {Z.z}] TTSテキスト', value: 'ボイス調整\nX=ボイス種：{0-3(自然),4-7(ロボ)}\nY.y=ピッチ{-10.0～+10.0}\nZ.z=再生速度{0.25～2.5}'}
				);
			message.channel.send({ embed }).catch(DumpError);
			console.log(GetTimeStamp() + "ACTION: sent embed message");
			return;	
		
		case "!disconnect":
			console.log(GetTimeStamp() + "CMD: disconnect");
			DisconnectFromVC(Bot, connection);
			console.log(GetTimeStamp() + "ACTION: disconnect done");
			return;
			
		case "!connect":
			console.log(GetTimeStamp() + "CMD: connect");
			ConnectToVC(Bot);
			console.log(GetTimeStamp() + "ACTION: connected done");
			return;
			
		case "!re":
		case "!reconnect":
			DisconnectFromVC(Bot, connection);
			await sleep(1000);
			ConnectToVC(Bot);
			return;
			
		case "!dictionary":
			console.log(GetTimeStamp() + "CMD: dictionary");
			DictionaryJSON();
			return;
			
		case "!test":
			console.log(GetTimeStamp() + "CMD: sample voice");
			if(!connection){ console.log(GetTimeStamp() + "WARN: no connection found"); return; }
			Dispatcher = connection.play('output.mp3', { volume: 0.5 });
			console.log(GetTimeStamp() + "ACTION: played test sound");
			return;
			
		case "!sc":
			console.log(GetTimeStamp() + "CMD: sound clip");
			
			if(!parsed[1])
			{
				console.log(GetTimeStamp() + "WARN: SC command not found");
				
				let embed = new Discord.MessageEmbed()
					.setColor('#0099ff')
					.setAuthor(Bot.user.username, Bot.user.displayAvatarURL('png'))
					.setTitle('SC Command List');
				
				fs.readdir("sounds", (err, files) => {
					let str = '';
					files.forEach(file => {
						console.log(GetTimeStamp() + "ACTION: found file " + file);
						//embed.addField('\u200b', file.split('.')[0], true);
						str += file.split('.')[0] + ' ';
					});
					embed.addField('\u200b', str, true);
					
					message.channel.send({ embed }).catch(DumpError);
					console.log(GetTimeStamp() + "ACTION: sent embed message");
				});
				return;
			}
			
			dispatcher = connection.play(
				`sounds/${parsed[1]}.wav`,
				{
					type: 'unknown',
					volume: 0.5
				});
			console.log(GetTimeStamp() + "ACTION: played sc sound " + parsed[1]);
			return;
			
		case "!yt":
			return; // 今はここで返す
			console.log(GetTimeStamp() + "CMD: youtube");
			
			if(!parsed[1]) { console.log(GetTimeStamp() + "WARN: yt addresss not found"); return; }
			if(!connection){ console.log(GetTimeStamp() + "WARN: no connection found"); return; }
			
			Dispatcher = connection.play(
				Ytdl(
					parsed[1],
					{
						filter: 'audioonly',
						quality: 'highestaudio'
					}
				),
				{
					type: 'unknown',
					volume: 0.1
				});
			console.log(GetTimeStamp() + "ACTION: played yt");
			return;
			
		case "!stop":
			if(Dispatcher) {
				console.log(GetTimeStamp() + "ACTION: stop disptacher");
				Dispatcher.pause();
			}
			return;

		default:
			if(message.channel.id !== process.env.DISCORD_GENERAL_ID)
			{
				console.log(GetTimeStamp() + "WARN: text not in general chat");
				return; 
			}
			
			if(parsed[0].charAt(0) == '!')
			{
				// other command check
				// ダイス
				let diceRegExp = /^!(\d*)d(\d+)/g;
				let diceMatch = diceRegExp.exec(message.content);
				
				if(!diceMatch || diceMatch.length < 2) return;
				console.log(diceMatch);
				
				let diceCount = parseInt(diceMatch[1]) ? parseInt(diceMatch[1]) : 1;
				let diceSides = parseInt(diceMatch[2]) ? parseInt(diceMatch[2]) : 6;
				
				diceCount = Math.floor(Math.min(Math.max(diceCount, 0), 100));
				diceSides = Math.floor(Math.min(Math.max(diceSides, 0), 10000));

				let total = 0;
				let totalStr = "=";
				for(let i = 0; i < diceCount; i++){
					let result = Math.floor(Math.random() * diceSides) + 1;
					total += result;
					totalStr += " " + result + " +";
				}
				totalStr = totalStr.slice(0, -1);
				
				let embed = new Discord.MessageEmbed()
				.setColor('#0099ff')
				.setAuthor(Bot.user.username, Bot.user.displayAvatarURL('png'))
				.setTitle("ダイス結果(" + diceCount + "d" + diceSides + ")")
				.addField("= " + total, totalStr);
				
				message.channel.send({ embed }).catch(DumpError);
				console.log(GetTimeStamp() + "ACTION: sent embed message");				
				return;
			}
			
			console.log(GetTimeStamp() + "CMD: text to speech");
			let text = message.content
				.replace(/https?:\/\/\S+/g, '')
				.replace(/<a?:.*?:\d+>/g, '')   // カスタム絵文字を除去
				.replace(/\n/g, '。')			// 改行を区切りに変更
				.replace('門松', 'もんまつ')
				.replace(/<@!\d+>/g, '')
				.replace(/<@\d+>/g, '')
				.replace(/<#\d+>/g, '')
				.slice(0, 100);					// 文字数制限（長すぎるとTTSの金がかかる）
				
			// Don't do anything if text is empty
			if(!text) { console.log(GetTimeStamp() + "WARN: text is empty"); return; }

			let voiceType = 0;
			let voicePitch = 0.0;
			let voiceSpeed = 1.1;
			if(text.charAt(0) == '['){
			let voiceRegExp = /\[\s*(\d+)\s*,?\s*([\-,\+]?\s*\d+\.?\d*)?\s*,?\s*([\-,\+]?\s*\d+\.?\d*)?\s*\]/g;
				let voiceMatch = voiceRegExp.exec(text);
				if(voiceMatch && voiceMatch.length > 0){
					voiceType = voiceMatch.length > 1 ? parseInt(voiceMatch[1]) : 0;
					voicePitch = voiceMatch.length > 2 ? parseFloat(voiceMatch[2]) : 0.0;
					voiceSpeed = voiceMatch.length > 3 ? parseFloat(voiceMatch[3]) : 1.1;
					
					text = text.replace(voiceMatch[0], "");
				}
			}
			
			text = NameReplace(message.member.user) + "。" + text;
			
			voicePitch = Math.min(Math.max(voicePitch, -10.0), 10.0);
			voiceSpeed = Math.min(Math.max(voiceSpeed, 0.25), 2.5);
			await ReadTTS(Bot, connection, text, voiceType, voicePitch, voiceSpeed);
			return;
	}
});


async function PlayYT(connection, url)
{
	if(!connection){ console.log(GetTimeStamp() + "WARN: no connection found"); return; }
	Dispatcher = connection.play(await Ytdl(url), { type: 'opus' });
	
	Dispatcher.on('error', error =>
	{
		console.log(error)
	});
}

async function ReadTTS(client, connection, text, voiceType = 0, voicePitch = 0.0, voiceSpeed = 1.1)
{
	if(!connection){ console.log(GetTimeStamp() + "WARN: no connection found"); return; }
	
	response = await TextToSpeechBase64(text, voiceType, voicePitch, voiceSpeed);
	if(response != null)
	{
		Dispatcher = connection.play(response, { volume: 1.0 });
		console.log(GetTimeStamp() + "ACTION: TTS done");
	}
	else
	{
		console.log(GetTimeStamp() + "ACTION: No TTS");
	}
}

function sleep(ms) {
	return new Promise((resolve) => {
		setTimeout(resolve, ms);
	});
}

function DumpError(err) {
	if (typeof err === "object") {
		if (err.message) {
			console.log("\nMessage: " + err.message)
		}
		if (err.stack) {
			console.log("\nStacktrace:")
			console.log("====================")
			console.log(err.stack);
		}
	} else {
		console.log("dumpError :: argument is not an object");
	}
}

// Login
Bot.login(process.env.BOT_TOKEN);